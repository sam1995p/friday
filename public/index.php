<?php 

require __DIR__ . '/vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('Application');
$log->pushHandler(new StreamHandler('logs/app.log', Logger::NOTICE));

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

$generator = new \Nubs\RandomNameGenerator\Vgng();
$name = $generator->getName();

$template = $twig->loadTemplate('showName.html.twig');
echo $template->render(
    array(
        'name' => $name,
        'ip' => $_SERVER['REMOTE_ADDR']
    )
);

$log->addNotice('User '.$_SERVER['REMOTE_ADDR'].' generated '.$name);